package com.storedproceduredemo.database.models;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
@Table(name = "project")
@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(name = "find_all", procedureName = "find_all", resultClasses = Project.class),
		@NamedStoredProcedureQuery(name = "find_project_by_id",resultClasses = Project.class, procedureName = "find_project_by_id", parameters = {
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "id", type = BigInteger.class) 
		}) 
})
public class Project {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "project_id")
	private Long projectId;

	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;

	public Project() {
	}

	public Project(BigInteger projectId, String title, String description) {
		super();
		this.projectId = projectId.longValue();
		this.title = title;
		this.description = description;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
