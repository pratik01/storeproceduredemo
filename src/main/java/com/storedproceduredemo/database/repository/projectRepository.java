package com.storedproceduredemo.database.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.storedproceduredemo.database.models.Project;

public interface projectRepository extends JpaRepository<Project, Serializable> {		
		
}
