package com.storedproceduredemo.database.services;

import java.math.BigInteger;
import java.util.List;

import com.storedproceduredemo.database.models.Project;

public interface ProjectService {
	
	public Project save(Project entity);
	public List<Project> findAll();
	public Project findOne(BigInteger projectId);
}
