package com.storedproceduredemo.database.services.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.storedproceduredemo.database.models.Project;
import com.storedproceduredemo.database.repository.projectRepository;
import com.storedproceduredemo.database.services.ProjectService;

@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private projectRepository projectRepository;

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> findAll() {
		// TODO Auto-generated method stub	
		StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("find_all");
		List<Object[]> storedProcedureResults = storedProcedure.getResultList();
		return storedProcedureResults.stream().map(result -> new Project((BigInteger) result[0], (String) result[1],(String) result[2]))
				.collect(Collectors.toList());
	}

	@Override
	public Project save(Project entity) {
		// TODO Auto-generated method stub
		return projectRepository.save(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Project findOne(BigInteger projectId) {
		// TODO Auto-generated method stub
		StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("find_project_by_id");
		storedProcedure.registerStoredProcedureParameter("id", BigInteger.class, ParameterMode.IN);
		storedProcedure.setParameter("id",projectId);
		List<Object[]> storedProcedureResults = storedProcedure.getResultList();
		Project project = null;
		if(storedProcedureResults.size()>0) {
			Object obj[] = storedProcedureResults.get(0);
			project = new Project((BigInteger) obj[0], (String) obj[1],(String) obj[2]);
		}
		return project;
	}

}
