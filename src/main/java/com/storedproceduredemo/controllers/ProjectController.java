package com.storedproceduredemo.controllers;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.storedproceduredemo.database.models.Project;
import com.storedproceduredemo.database.services.ProjectService;

@RestController
public class ProjectController {

	@Autowired
	private ProjectService projectService;

	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public List<Project> findAll() {
		return projectService.findAll();
	}

	@RequestMapping(value = "/projects", method = RequestMethod.POST)
	public Project save(@RequestBody Project entity) {
		return projectService.save(entity);
	}
	
	@RequestMapping(value = "/projects/{id}", method = RequestMethod.GET)
	public Project save(@PathVariable("id") BigInteger id) {
		return projectService.findOne(id);
	}
}
